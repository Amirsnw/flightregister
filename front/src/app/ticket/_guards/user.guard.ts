﻿import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {UserTransfer} from "../_models";

@Injectable({providedIn: 'root'})
export class UserGuard implements CanActivate {

  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = <UserTransfer>JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.roles.includes("USER")) {
      return true;
    }

    // not logged in so redirect to login page
    this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
    return false;
  }
}
