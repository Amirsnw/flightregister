﻿import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {UserTransfer} from "../_models";

@Injectable({providedIn: 'root'})
export class AdminGuard implements CanActivate {

  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = <UserTransfer>JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.roles.includes("ADMIN")) {
      return true;
    }

    // not logged in so redirect to login page
    this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
    return false;
  }
}
