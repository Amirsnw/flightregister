﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '../_services';

@Component({templateUrl: 'login.component.html'})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService) {}

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
        /* or
        this.loginForm = new FormGroup({
          username: new FormControl(''),
          password: new FormControl(''),
        });
         */

        // reset login status
        this.authenticationService.logout();

        // for backing to previous page
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    }

    // getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;
        if (this.loginForm.invalid) {
            return;
        }
        //this.authenticationService.logout();

        this.loading = true;
        this.authenticationService.login(this.f.username.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                // if login was successful
                data => {
                    this.router.navigate([this.returnUrl]);
                },
                // login was not successful
                error => {
                  if(error === 'Unprocessable Entity') {
                    this.error = "Invalid Usrname/Password"
                  } else {
                    this.error = error;
                  }
                    this.loading = false;
                });
    }
}
