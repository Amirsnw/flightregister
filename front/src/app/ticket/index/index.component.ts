import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  logOutMessage: string = '';

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.logOutMessage = this.route.snapshot.queryParams['logOutMessage'] || '';
    setTimeout(() => { this.logOutMessage = ''; }, 2000);
  }
}
