﻿import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';

import {Customer, Ticket} from '../_models';
import {throwError} from "rxjs";
import {catchError, last, map} from "rxjs/operators";
import {Airplane} from "../_models/airplane";
import {error} from "@angular/compiler/src/util";

@Injectable({ providedIn: 'root' })
export class TicketService {
    constructor(private http: HttpClient) { }
  /*_username: string;
  set username(username: string){
    this._username = username;
  }*/

  getUserTickets(username: string) {
    const options = { params: new HttpParams().set('username', username) };
    return this.http.get<Ticket[]>(`api/ticket`, options).pipe(
      catchError(error => this.handleError(error)));
  }

  getAllTickets(ticketForm: any) {
    const options = { params: new HttpParams().set('getBy', ticketForm['getBy'])
                      .set('sortBy', ticketForm['sortBy'])
                      .set('param', ticketForm['param'])
                    };
    return this.http.get<Ticket[]>(`api/alltickets`, options).pipe(
      catchError(error => this.handleError(error)));
  }

  getAllAirplanes() {
    return this.http.get<Airplane[]>(`api/airplane`).pipe(
      /*catchError((err,caught)  => {
        this.handleError(err);
        return caught;
      }));*/
      catchError(error => this.handleError(error)));
  }

  registerTicket(registerForm: any) {
    /*let formData: FormData = new FormData();
    formData.append('ticket.from', registerForm['from']);
    formData.append('ticket.to', registerForm['to']);
    formData.append('ticket.airplaneId', registerForm['airplaneId']);
    formData.append('ticket.flightClass', registerForm['flightClass']);
    formData.append('customer.name', registerForm['name']);
    formData.append('customer.family', registerForm['family']);
    formData.append('customer.nationalCode', registerForm['nationalCode']);
    formData.append('seat', registerForm['seat']);*/

    // Wrapping form data
    let ticket: Ticket = new Ticket();
    let customer: Customer = new Customer();

    ticket.from = registerForm['from'] === "" ? null : registerForm['from'];
    ticket.to = registerForm['to'] === "" ? null : registerForm['to'];
    ticket.airplaneId = registerForm['airplaneId'];
    ticket.flightClass = registerForm['flightClass'] === "" ? null : registerForm['flightClass'];

    customer.name = registerForm['name'];
    customer.family = registerForm['family'];
    customer.nationalCode = registerForm['nationalCode'];

    let registerData = {
      customer: customer,
      ticket: ticket,
      seat: registerForm['seat']
    }

    /*const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      }),
      responseType: 'json'
    };*/

    /*httpOptions.headers =
      httpOptions.headers.set('Content-Type', 'content-type=multipart');*/

    const headers = new HttpHeaders().set('Content-Type', 'application/json');

    const formJson: string = JSON.stringify(registerData);

    return this.http.post<any>(`api/register/ticket`, formJson, {headers,
      responseType:"json",
      observe: 'body'})
      .pipe(
        /*map(message => {
        console.log("message: " + message);
        return message;
      }),*/
        catchError(error => this.handleError(error)));

  }

  registerAirplane(registerForm: any) {
    let airplane: Airplane = new Airplane();

    airplane.airplaneType = registerForm['airplaneType'] === "" ? null : registerForm['airplaneType'];
    airplane.capacity = registerForm['capacity'];
    airplane.terminalNumber = registerForm['terminalNumber'];
    airplane.price = registerForm['price'];

    const headers = new HttpHeaders().set('Content-Type', 'application/json');

    const formJson: string = JSON.stringify(airplane);

    return this.http.post<any>(`api/register/airplane`, formJson, {headers,
      responseType:"json",
      observe: 'body'})
      .pipe(
        /*map(message => {
        console.log("message: " + message);
        return message;
      }),*/
        catchError(error => this.handleError(error)));

  }

  private handleError(error: HttpErrorResponse) {
    if(error) {
      return throwError(error);
    }

    return throwError(
      'Something happened; try again later.');
  }
}
