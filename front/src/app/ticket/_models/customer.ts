export class Customer {
  id: number;
  name: string;
  family: string;
  nationalCode: number;
  username: string;
}
