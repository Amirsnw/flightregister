export class Airplane {
  id: number;
  capacity: number;
  airplaneType: string;
  terminalNumber: number;
  price: number;
}
