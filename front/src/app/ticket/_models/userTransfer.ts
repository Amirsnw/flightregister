﻿export class UserTransfer {
    username: string;
    firstName: string;
    lastName: string;
    roles: string[];
    token: string;
    status: string;
    authdata: string;
}
