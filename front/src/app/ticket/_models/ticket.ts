export class Ticket {
  id: number;
  date: number;
  customerId: number;
  from: string;
  to: string;
  airplaneId: string;
  flightClass: string;
  copon: string;
  airLine: string;
  createDate: string;
}
