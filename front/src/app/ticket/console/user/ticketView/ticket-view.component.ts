﻿import {Component, OnInit} from '@angular/core';

import {Ticket, UserTransfer} from '../../../_models';
import {TicketService} from '../../../_services';

@Component({templateUrl: 'ticket-view.component.html'})
export class TicketViewComponent implements OnInit {
  tickets: Ticket[];

  constructor(private ticketService: TicketService) {
  }

  ngOnInit(): void {
    this.showTable();
  }

  showTable(): void {
    let user: UserTransfer = JSON.parse(localStorage.getItem('currentUser'));
    this.ticketService.getUserTickets(user.username)
      .subscribe((tickets: any) => {
        this.tickets = tickets;
      });
  }
}
