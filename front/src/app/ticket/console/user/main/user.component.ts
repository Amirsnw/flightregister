import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthenticationService, TicketService} from "../../../_services";
import {Airplane} from "../../../_models/airplane";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  airplanes: Airplane[];
  error: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private ticketService: TicketService,
    private authenticationService: AuthenticationService) {
  }

  // getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(4),Validators.pattern('^[a-zA-Z]+')]],
      family: ['', [Validators.required, Validators.minLength(4),Validators.pattern('^[a-zA-Z]+')]],
      nationalCode: ['', [Validators.required, Validators.pattern('^[0-9]{10}$')]],
      from: ['', Validators.required],
      to: ['', Validators.required],
      airplaneId: ['', [Validators.required]],
      flightClass: ['', Validators.required],
      seat: ['', [Validators.required, Validators.pattern('^(?:[1-9]|[1-4][0-9]|50)')]],
    });
    this.formProvider();
  }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    this.ticketService.registerTicket(this.registerForm.value)
      .subscribe(
        // if login was successful
        response => {
          console.log("Response: " + response);
          this.router.navigateByUrl("/user/view");
        },
        // login was not successful
        error => {
          console.warn("Error: " + error.status);
          this.error = error;
          this.loading = false;
        });
  }

  formProvider() {
    this.ticketService.getAllAirplanes().subscribe(
      planes => {
        this.airplanes = planes;
      }
      ,
      error => {
        this.error = error;
      });
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigateByUrl("/console");
  }
}
