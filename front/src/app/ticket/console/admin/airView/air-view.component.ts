﻿import {Component, OnInit} from '@angular/core';

import {Airplane} from '../../../_models';
import {TicketService} from '../../../_services';

@Component({templateUrl: 'air-view.component.html'})
export class AirViewComponent implements OnInit {
  airplanes: Airplane[];

  constructor(private ticketService: TicketService) {
  }

  ngOnInit(): void {
    this.showTable();
  }

  showTable(): void {
    this.ticketService.getAllAirplanes()
      .subscribe(
        // if login was successful
        response => {
          this.airplanes = response;
        },
        // login was not successful
        error => {
          console.warn("Error: " + error.status);
        });
  }
}
