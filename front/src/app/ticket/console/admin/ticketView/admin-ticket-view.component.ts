﻿import {Component, OnInit} from '@angular/core';

import {Airplane, Customer, Ticket, UserTransfer} from '../../../_models';
import {TicketService} from '../../../_services';

@Component({templateUrl: 'admin-ticket-view.component.html'})
export class AdminTicketViewComponent implements OnInit {
  tickets: Ticket[];
  customers: Customer[];
  airplanes: Airplane[];

  constructor(private ticketService: TicketService) {
  }

  ngOnInit(): void {
    let ticketMap = JSON.parse(localStorage.getItem('ticketMap'));
    this.tickets = ticketMap["tickets"];
    this.customers = ticketMap["customers"];
    this.airplanes = ticketMap["airplanes"];
  }

}
