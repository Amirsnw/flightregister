import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Airplane} from "../../../_models";
import {Router} from "@angular/router";
import {AuthenticationService, TicketService} from "../../../_services";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  ticketForm: FormGroup;
  airplaneRegForm: FormGroup;
  loading = false;
  submitted = false;
  ticket: Airplane[];
  error: any;
  success: string = '';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private ticketService: TicketService,
    private authenticationService: AuthenticationService) {
  }

  // getter for easy access to ticket form fields
  get f() {
    return this.ticketForm.controls;
  }

  // getter for easy access to airplane form fields
  get f2() {
    return this.airplaneRegForm.controls;
  }

  ngOnInit(): void {
    setTimeout(() => { this.success = ''; }, 2000);

    this.ticketForm = this.formBuilder.group({
      getBy: ['all', Validators.required],
      param: [''],
      sortBy: ['customerName', Validators.required]
    });
    this.airplaneRegForm = this.formBuilder.group({
      airplaneType: ['', Validators.required],
      capacity: ['', [Validators.required, Validators.pattern("^(?:[1-9]|[1-4][0-9][0-9]|[0-9][0-9]|500)")]],
      terminalNumber: ['', [Validators.required, Validators.pattern("[0-9]{3}")]],
      price: ['', [Validators.required, Validators.pattern("^[0-9]+")]]
    });
  }

  ticketOnSubmit() {
    this.submitted = true;
    if (this.ticketForm.invalid) {
      return;
    }

    this.loading = true;
    this.ticketService.getAllTickets(this.ticketForm.value)
      .subscribe(
        // if login was successful
        response => {
          localStorage.setItem('ticketMap', JSON.stringify(response));
          this.router.navigateByUrl("/admin/view/ticket");
        },
        // login was not successful
        error => {
          console.warn("Error: " + error.status);
          this.error = error;
          this.loading = false;
        });
  }

  airplaneOnSubmit() {
    this.submitted = true;
    if (this.airplaneRegForm.invalid) {
      return;
    }

    this.loading = true;
    this.ticketService.registerAirplane(this.airplaneRegForm.value)
      .subscribe(
        // if login was successful
        response => {
          this.success = "Airplane Registered";
          this.loading = false;
        },
        // login was not successful
        error => {
          console.warn("Error: " + error.status);
          this.error = error;
          this.loading = false;
        });
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(["/console"], {queryParams: {logOutMessage: "You Have Logged Out"}});
  }

}
