import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import { Router } from '@angular/router';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TokenAuthInterceptor, ErrorInterceptor} from "./ticket/_helpers";
import {LoginComponent} from "./ticket/login";
import {TicketViewComponent} from "./ticket/console/user/ticketView";
import {TicketService} from "./ticket/_services";
import { IndexComponent } from './ticket/index';
import { UserComponent } from './ticket/console/user/main';
import {AirViewComponent} from "./ticket/console/admin/airView";
import {AdminComponent} from "./ticket/console/admin/main";
import {AdminTicketViewComponent} from "./ticket/console/admin/ticketView";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    IndexComponent,
    UserComponent,
    TicketViewComponent,
    AdminComponent,
    AirViewComponent,
    AdminTicketViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenAuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

    TicketService],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(router: Router) {}
}
