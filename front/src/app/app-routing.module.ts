import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminGuard, UserGuard} from "./ticket/_guards";
import {LoginComponent} from "./ticket/login";
import {TicketViewComponent} from "./ticket/console/user/ticketView";
import {IndexComponent} from "./ticket/index";
import {UserComponent} from "./ticket/console/user/main";
import {AirViewComponent} from "./ticket/console/admin/airView";
import {AdminComponent} from "./ticket/console/admin/main";
import {AdminTicketViewComponent} from "./ticket/console/admin/ticketView";

const routes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'login', component: LoginComponent },
  { path: 'user/console', component: UserComponent, canActivate: [UserGuard] },
  { path: 'user/view', component: TicketViewComponent, canActivate: [UserGuard] },
  { path: 'admin/console', component: AdminComponent, canActivate: [AdminGuard] },
  { path: 'admin/view/ticket', component: AdminTicketViewComponent, canActivate: [AdminGuard] },
  { path: 'admin/view/airplane', component: AirViewComponent, canActivate: [AdminGuard] },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
