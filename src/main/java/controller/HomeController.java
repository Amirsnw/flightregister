package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The usecase-controller for home page
 *
 * @author Amir Khalighi
 */
@Controller
public class HomeController {

    @RequestMapping({"/", "/*"})
    public String showHomePage() {

        /* For Production */
        return "index";

        /* For Developing Mode */
        //return "redirect:http://localhost:4200";
    }

    @RequestMapping("/logout")
    public String logOut(HttpServletRequest request) {
        return "index";
    }
}
