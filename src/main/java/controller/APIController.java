package controller;

import config.security.TokenUtil;
import exception.FlightNotFoundException;
import model.front.FormObject;
import model.flight.AirLine;
import model.flight.Airplane;
import model.flight.FlightClass;
import model.front.MessageResponse;
import model.front.security.request.AuthenticationRequest;
import model.front.security.response.UserTransfer;
import model.ticket.Copon;
import model.ticket.Ticket;
import model.user.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import service.api.AirplaneService;
import service.api.CustomerService;
import service.api.TicketService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.*;

/**
 * The usecase-controller for ticket register form for user
 *
 * @author Amir Khalighi
 */
@RestController
@RequestMapping({"/api"})
public class APIController {

    @Autowired
    UserDetailsService userService;
    @Autowired
    private TicketService ticketService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private AirplaneService airplaneService;
    @Autowired
    private AuthenticationManager authenticationManager;

    @RequestMapping(path = "/ticket", method = RequestMethod.GET, produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<List<Ticket>> allTickets(@RequestParam String username, HttpServletRequest request) {
        System.out.println("Api Called");
        List<Ticket> tickets;
        tickets = ticketService.getTicketsByUsername(username);
        return ResponseEntity.ok().body(tickets);
    }

    @RequestMapping(path = "/airplane", method = RequestMethod.GET, produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<List<Airplane>> allAirplanes(HttpServletRequest request) {
        System.out.println("Api Called");
        List<Airplane> airplanes;
        airplanes = airplaneService.getAllAirplanes();
        return ResponseEntity.ok().body(airplanes);
        /*
        return new ResponseEntity<List<Airplane>>(airplanes, HttpStatus.OK);
        */
    }

    @GetMapping(path = "/alltickets")
    public ResponseEntity showTickets(Model model,
                                      @RequestParam("getBy") String getBy,
                                      @RequestParam("sortBy") String sortBy,
                                      @RequestParam("param") String param,
                                      HttpServletRequest request) {
        long id = 0;
        String name = "";
        List<Ticket> tickets = new LinkedList<>();
        Map<Long, Airplane> airplanes = new HashMap<>();
        Map<Long, Customer> customers = new HashMap<>();
        HttpSession session = request.getSession();

        try {
            switch (getBy) {
                case "ticketId":
                    try {
                        id = Long.parseLong(param);
                    } catch (NumberFormatException e) {
                        return new ResponseEntity<MessageResponse>(new MessageResponse(e.getMessage(),
                                HttpStatus.UNPROCESSABLE_ENTITY), HttpStatus.UNPROCESSABLE_ENTITY);
                    }
                    if (id > 0) {
                        Ticket ticket = ticketService.getTicketById(id);
                        if (ticket != null) {
                            tickets.add(ticket);
                        }
                    }
                    break;
                case "customerName":
                    if (!param.equals("")) {
                        tickets = ticketService.getTicketsByCustomerName(param);
                    }
                    break;
                default:
                    tickets = ticketService.getAllTickets();
            }
        } catch (FlightNotFoundException e) {
            return new ResponseEntity<MessageResponse>(new MessageResponse(e.getMessage(),
                    HttpStatus.EXPECTATION_FAILED), HttpStatus.EXPECTATION_FAILED);
        }
        switch (sortBy) {
            case "customerName":
                tickets = ticketService.sortByName(tickets);
                break;
            case "bookingDate":
                tickets = ticketService.sortByBookingDate(tickets);
                break;
        }

        try {
            for (Ticket ticket : tickets) {
                customers.put(ticket.getId(), customerService.getCustomerById(ticket.getCustomerId()));
                airplanes.put(ticket.getId(), airplaneService.getAirplaneById(ticket.getAirplaneId()));
            }
        } catch (Exception e) {
            return new ResponseEntity<MessageResponse>(new MessageResponse(e.getMessage(),
                    HttpStatus.EXPECTATION_FAILED), HttpStatus.EXPECTATION_FAILED);
        }


        Map<String,Object> map = new HashMap<>();
        map.put("tickets", tickets);
        map.put("customers", customers);
        map.put("airplanes", airplanes);

        return new ResponseEntity<Map<String,Object>>(map, HttpStatus.OK);
    }

    @RequestMapping(value = "/register/ticket", method = RequestMethod.POST)
    public ResponseEntity registerTicket(@Valid @RequestBody FormObject formObject, BindingResult bindingResult,
                                         HttpServletRequest request) {

        if (bindingResult.hasErrors()) {
            /*List<String> errors = bindingResult.getAllErrors().stream().
                    map(e -> {
                                if(e instanceof FieldError) {
                                    FieldError fieldError = (FieldError) e;
                                    return fieldError.getField() + ":" + e.getDefaultMessage();
                                }
                        return null;
                    }).collect(Collectors.toList());*/

            /*Map<String,String> errors = bindingResult.getAllErrors().stream().
                    collect(Collectors.toMap(e -> ((FieldError)e).getField(), e -> ((FieldError)e).getDefaultMessage()));*/

            Map<String, String> errors = new HashMap<>();
            for (ObjectError error : bindingResult.getAllErrors()) {
                if (error instanceof FieldError) {
                    FieldError fieldError = (FieldError) error;
                    errors.put(fieldError.getField(), fieldError.getDefaultMessage());
                }
            }
            //System.out.println(bindingResult.getModel().get("org.springframework.validation.BindingResult.formObject"));
            return new ResponseEntity<MessageResponse>(new MessageResponse(errors.toString(),
                    HttpStatus.UNPROCESSABLE_ENTITY), HttpStatus.UNPROCESSABLE_ENTITY);
        }

        // Initializing entities
        final AirLine airLine = new AirLine("Iran Air");
        final Copon copon = new Copon(30, "Thirty Percent");
        final Customer customer = new Customer(formObject.customer.getName(), formObject.customer.getFamily(),
                formObject.customer.getNationalCode(), request.getRemoteUser());
        final FlightClass flightClass = formObject.ticket.getFlightClass();
        final Date flightDate = new Date();
        try {
            ticketService.registerTicket(flightDate,
                    customer, formObject.ticket.getFrom(), formObject.ticket.getTo(),
                    airLine, formObject.ticket.getAirplaneId(), flightClass, copon, formObject.getSeat());
        } catch (Exception e) {
            return new ResponseEntity<MessageResponse>(new MessageResponse(e.getMessage(),
                    HttpStatus.NOT_ACCEPTABLE), HttpStatus.NOT_ACCEPTABLE);
        }

        return new ResponseEntity<MessageResponse>(new MessageResponse("Ticket Registered", HttpStatus.OK), HttpStatus.OK);
    }

    @RequestMapping(value = "/register/airplane", method = RequestMethod.POST)
    public ResponseEntity registerAirplane(@Valid @RequestBody Airplane airplane, BindingResult bindingResult,
                                   HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            Map<String, String> errors = new HashMap<>();
            for (ObjectError error : bindingResult.getAllErrors()) {
                if (error instanceof FieldError) {
                    FieldError fieldError = (FieldError) error;
                    errors.put(fieldError.getField(), fieldError.getDefaultMessage());
                }
            }
            return new ResponseEntity<MessageResponse>(new MessageResponse(errors.toString(),
                    HttpStatus.UNPROCESSABLE_ENTITY), HttpStatus.UNPROCESSABLE_ENTITY);
        }

        try {
            airplaneService.registerAirplane(airplane.getAirplaneType(), airplane.getCapacity(),
                    airplane.getPrice(), airplane.getTerminalNumber());
        } catch (IOException e) {
            return new ResponseEntity<MessageResponse>(new MessageResponse(e.getMessage(),
                    HttpStatus.EXPECTATION_FAILED), HttpStatus.EXPECTATION_FAILED);
        }

        return new ResponseEntity<MessageResponse>(new MessageResponse("Airplane Registered", HttpStatus.OK), HttpStatus.OK);
    }

    @RequestMapping(value = "/authenticate", method = {RequestMethod.POST})
    public ResponseEntity<UserTransfer> authenticate(AuthenticationRequest authenticationRequest) {
        try {
            String username = authenticationRequest.getUsername();
            String password = authenticationRequest.getPassword();
            /*BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String encodedPassword = passwordEncoder.encode(password);*/

            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            Authentication authentication = this.authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            UserDetails userDetails = this.userService.loadUserByUsername(username);

            List<String> roles = new ArrayList();
            for (GrantedAuthority authority : userDetails.getAuthorities()) {
                roles.add(authority.toString());
            }

            return new ResponseEntity<UserTransfer>(new UserTransfer(userDetails.getUsername(), roles,
                    TokenUtil.createToken(userDetails), HttpStatus.OK), HttpStatus.OK);

        } catch (BadCredentialsException bce) {
            System.out.println("Login Failed");
            return new ResponseEntity<UserTransfer>(new UserTransfer(), HttpStatus.UNPROCESSABLE_ENTITY);

        } catch (Exception e) {
            System.out.println("EXPECTATION_FAILED");
            //e.printStackTrace();
            return new ResponseEntity(HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping("/logout")
    public ResponseEntity logOut(HttpServletRequest request) {
        System.out.println("Api Called");
        return new ResponseEntity<MessageResponse>(new MessageResponse("Logged Out",
                HttpStatus.OK), HttpStatus.OK);
    }
}

