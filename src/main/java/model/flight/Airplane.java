/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package model.flight;

import baseData.AirplaneType;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.io.Serializable;

/**
 * @author Amir Khalighi
 */

public class Airplane implements Serializable {

    private long id;

    @NotNull
    private AirplaneType airplaneType;

    @Positive
    @Max(value = 500, message = "Capacity must be lower or equal to 500")
    private int capacity;

    @Positive
    private long price;

    @Positive
    @Digits(fraction = 0, integer = 3, message = "Terminal Number must be at least 3 digits")
    private int terminalNumber;


    /**
     * Two parameter constructor
     */
    public Airplane(AirplaneType airplaneType, int capacity, long price, int terminalNumber) {
        this.capacity = capacity;
        this.airplaneType = airplaneType;
        this.terminalNumber = terminalNumber;
        this.price = price;
    }

    /**
     * constructor with an entity parameter
     */
    public Airplane(Airplane airplane) {
        this.id = airplane.getId();
        this.capacity = airplane.getCapacity();
        this.airplaneType = airplane.getAirplaneType();
        this.terminalNumber = airplane.getTerminalNumber();
        this.price = airplane.getPrice();
    }

    /**
     * none parameter, default constructor
     */
    public Airplane() {
        super();
    }

    //Setter methods
    public void setId(long id) {
        this.id = id;
    }

    public void setAirplaneType(AirplaneType airplaneType) {
        this.airplaneType = airplaneType;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public void setTerminalNumber(int terminalNumber) {
        this.terminalNumber = terminalNumber;
    }

    //Getter methods
    public long getId() {
        return id;
    }

    public AirplaneType getAirplaneType() {
        return airplaneType;
    }

    public int getCapacity() {
        return capacity;
    }

    public long getPrice() {
        return price;
    }

    public int getTerminalNumber() {
        return terminalNumber;
    }

    @Override
    public String toString() {

        StringBuilder objStr = new StringBuilder("id=" + this.getId() + ", "
                + "airplaneType=" + this.getAirplaneType() + ", "
                + "capacity=" + this.getCapacity() + ", "
                + "terminalNumber=" + this.getTerminalNumber() + ", "
                + "price=" + this.getPrice());
        return objStr.toString();
    }
}
