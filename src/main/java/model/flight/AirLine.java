/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package model.flight;

import java.io.Serializable;

/**
 * The airline that sells ticket for flights.
 *
 * @author Amir Khalighi
 */
public class AirLine implements Serializable {
    private long id;
    private String airLineName;

    /**
     * One parameter constructor
     */
    public AirLine(String airLineName) {
        this.airLineName = airLineName;
    }

    public AirLine() {
        super();
    }

    //Setter methods
    public void setAirLineName(String name) {
        airLineName = name;
    }

    //Getter methods
    public String getAirLineName() {
        return airLineName;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "AirLine{" +
                "id=" + id +
                ", airLineName='" + airLineName + '\'' +
                '}';
    }
}
