/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package model.flight;

/**
 * This entity is for type of flights
 * that is gold or silver.
 *
 * @author Amir Khalighi
 */
public enum FlightClass {
    GOLD, SILVER
}
