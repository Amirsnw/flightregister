/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package model.user;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * For the customer who buy flight ticket.
 *
 * @author Amir Khalighi
 */
public class Customer implements Serializable {
    private long id;

    @NotBlank
    @Size(min = 3, max = 20, message = "Name must be between 3 and 20 characters long.")
    @Pattern(regexp = "^[a-zA-Z]+$", message = "Name must be alphanumeric with no spaces")
    private String name;

    @NotBlank
    @Size(min = 3, max = 20, message = "Family must be between 3 and 20 characters long.")
    @Pattern(regexp = "^[a-zA-Z]+$", message = "Family must be alphanumeric with no spaces")
    private String family;

    @Positive
    @Digits(fraction = 0, integer = 10, message = "National code must be 10 digits")
    @Min(value = 999999999, message = "National code must be 10 digits")
    private long nationalCode;

    private String username;

    /**
     * this constructor does not need a Ticket
     */
    public Customer(String name, String family, long nationalCode, String username) {
        this.name = name;
        this.family = family;
        this.nationalCode = nationalCode;
        this.username = username;
    }

    public Customer() {
        super();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Customer)) return false;

        Customer customer = (Customer) obj;
        return (this.name + " " + this.family).equals(customer.name + " " + customer.family);
    }

    //Setter methods
    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public void setNationalCode(long nationalCode) {
        this.nationalCode = nationalCode;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    //Getter methods
    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFamily() {
        return family;
    }

    public long getNationalCode() {
        return nationalCode;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public String toString() {

        StringBuilder objStr = new StringBuilder("customerId=" + this.getId() + ", "
                + "customerName=" + this.getName() + ", "
                + "customerFamily=" + this.getFamily() + ", "
                + "customerNationalCode=" + this.getNationalCode() + ", "
                + "username=" + this.getUsername());

        return objStr.toString();
    }
}
