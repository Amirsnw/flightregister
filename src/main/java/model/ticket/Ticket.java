/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package model.ticket;

import baseData.City;
import model.flight.AirLine;
import model.flight.FlightClass;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This entity is the off copon that will be used
 * to calculate the final ticket price
 *
 * @author Amir Khalighi
 */
public class Ticket implements Serializable {
    private long id;
    private Date date;

    private long customerId;

    @NotNull
    private City from;

    @NotNull
    private City to;

    @Positive
    private long airplaneId;

    @NotNull
    private FlightClass flightClass;

    private Copon copon;
    private AirLine airLine;
    private Date createDate;


    /**
     * Full parameter constructor without customer field.
     */
    public Ticket(Date date, long customerId, City from,
                  City to, Copon copon, AirLine airline, long airplaneId, FlightClass flightClass) {

        /* You need to set customer using its corresponding setter */
        this.date = date;
        this.customerId = customerId;
        this.from = from;
        this.to = to;
        this.copon = copon;
        this.airLine = airline;
        this.airplaneId = airplaneId;
        this.flightClass = flightClass;
        this.createDate = new Date();
		
		/*
		//Validation
		if(price > 0) {
			if(copon != null){
			   price= new TicketServiceImpl().calculateOffPrice(this.copon,price);
			}
		}
		*/
    }

    /**
     * constructor with an entity parameter
     */
    public Ticket(Ticket ticket) {

        this.id = ticket.getId();
        this.date = ticket.getDate();
        this.customerId = ticket.getCustomerId();
        this.from = ticket.getFrom();
        this.to = ticket.getTo();
        this.copon = ticket.getCopon();
        this.airLine = ticket.getAirLine();
        this.airplaneId = ticket.getAirplaneId();
        this.flightClass = ticket.getFlightClass();
        this.createDate = ticket.getCreateDate();
    }

    public Ticket() {
        super();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Ticket)) return false;

        Ticket ticket = (Ticket) obj;
        return this.id == ticket.id;
    }

    //Setter methods

    public void setId(long id) {
        this.id = id;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setAirplaneId(long airplaneId) {
        this.airplaneId = airplaneId;
    }
	
	
	/*
	public void setPrice(long price) {
		
		//validating 
		if(price > 0) {
		   if(copon != null) {
			  price = new TicketServiceImpl().calculateOffPrice(this.copon,price);
			}
		}
	}
	*/

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public void setFrom(City from) {
        this.from = from;
    }

    public void setTo(City to) {
        this.to = to;
    }

    public void setCopon(Copon copon) {
        this.copon = copon;
    }

    public void setAirLine(AirLine airLine) {
        this.airLine = airLine;
    }

    public void setFlightClass(FlightClass flightClass) {
        this.flightClass = flightClass;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    //Getter methods
    public long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public long getCustomerId() {
        return customerId;
    }

    public City getFrom() {
        return from;
    }

    public City getTo() {
        return to;
    }

    public Copon getCopon() {
        return copon;
    }

    public AirLine getAirLine() {
        return airLine;
    }

    public FlightClass getFlightClass() {
        return flightClass;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public long getAirplaneId() {
        return airplaneId;
    }


    @Override
    public String toString() {

        DateFormat dateFormatter = new SimpleDateFormat("dd-M-yyyy hh mm ss");

        StringBuilder objStr = new StringBuilder("id=" + this.getId() + ", "
                + "flightDate=" + dateFormatter.format(this.getDate()) + ", "
                + "customerId=" + this.getCustomerId() + ", "
                + "airLineName=" + this.getAirLine().getAirLineName() + ", "
                + "flightClass=" + this.getFlightClass() + ", "
                + "airplaneId=" + this.getAirplaneId() + ", "
                + "from=" + this.getFrom() + ", "
                + "to=" + this.getTo() + ", "
                + "offPercentage=" + this.getCopon().getOffPercentage() + ", "
                + "coponName=" + this.getCopon().getCoponName() + ", "
                + "createDate=" + dateFormatter.format(this.getCreateDate()));

        return objStr.toString();
    }
}
