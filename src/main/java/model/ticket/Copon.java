/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package model.ticket;

import java.io.Serializable;

/**
 * This entity is the off copon that will be used
 * to calculate the final ticket price
 *
 * @author Amir Khalighi
 */
public class Copon implements Serializable {
    private long id;
    private int offPercentage;
    private String coponName;

    /**
     * Two parameter constructor
     */
    public Copon(int offPercentage, String coponName) {
        this.offPercentage = offPercentage;
        this.coponName = coponName;
    }

    public Copon() {
        super();
    }

    //Setter methods
    public void setOffPercentage(int offPercentage) {
        this.offPercentage = offPercentage;
    }

    public void setCoponName(String coponName) {
        this.coponName = coponName;
    }

    //Getter methods
    public int getOffPercentage() {
        return offPercentage;
    }

    public String getCoponName() {
        return coponName;
    }

    public long getId() {
        return id;
    }
}
