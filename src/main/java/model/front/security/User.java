package model.front.security;

import java.io.Serializable;

public class User implements Serializable {

    public User(String firstName, String lastName, String username, String password, String email, String role) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.email = email;
        this.role = role;
    }

    /**
     * constructor with an entity parameter
     */
    public User(User user) {

        this.id = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.email = user.getEmail();
        this.role = user.getRole();
    }

    public User() {
        super();
    }

    private static final long serialVersionUID = 1L;

    private Long id;

    private String firstName;

    private String lastName;

    private String username;

    private String password;

    private String email;

    private String role;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {

        StringBuilder objStr = new StringBuilder("id=" + this.getId() + ", "
                + "firstName=" + this.getFirstName() + ", "
                + "lastName=" + this.getLastName() + ", "
                + "username=" + this.getUsername() + ", "
                + "password=" + this.getPassword() + ", "
                + "email=" + this.getEmail() + ", "
                + "role=" + this.getRole());

        return objStr.toString();
    }
}
