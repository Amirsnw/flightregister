package model.front;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import model.ticket.Ticket;
import model.user.Customer;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.io.Serializable;

/**
 * Wrapperfor Ticket and Customer used on controllers
 *
 * @author Amir Khalighi
 */
@JsonIgnoreProperties( ignoreUnknown=true )
public class FormObject implements Serializable {

    @NotNull
    @Valid
    public Ticket ticket;

    @NotNull
    @Valid
    public Customer customer;

    /* Other Fields */
    @Positive
    @Max(value = 50, message = "Seat number must be lower or equal to 50")
    public int seat;

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }
}
