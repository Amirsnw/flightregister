package model.front;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import model.ticket.Ticket;
import model.user.Customer;
import org.springframework.http.HttpStatus;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.io.Serializable;

/**
 * Wrapperfor Ticket and Customer used on controllers
 *
 * @author Amir Khalighi
 */
@JsonIgnoreProperties( ignoreUnknown=true )
public class MessageResponse implements Serializable {

    private String message;
    private  HttpStatus status;

    public MessageResponse(String message, HttpStatus status) {
        this.message = message;
        this.status = status;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }
}
