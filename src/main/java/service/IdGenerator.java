/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package service;

import model.flight.Airplane;
import model.ticket.Ticket;
import model.user.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import service.api.AirplaneService;
import service.api.CustomerService;
import service.api.TicketService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This is the Id Generator for all entities
 *
 * @author Amir Khalighi
 */
public class IdGenerator {

    TicketService ticketService;
    CustomerService customerService;
    AirplaneService airplaneService;
    private static Set<Long> bucket = new HashSet<>();
    private static final long MIN = 1L;
    private static final long MAX = 999999999L; // number length = 9

    /**
     * TicketService setter injection
     */
    @Autowired
    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    /**
     * CustomerService setter injection
     */
    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    /**
     * AirplaneService setter injection
     */
    @Autowired
    public void setAirplaneService(AirplaneService airplaneService) {
        this.airplaneService = airplaneService;
    }

    {
        boolean duplicate = false;

        if (MIN >= MAX) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        List<Ticket> tickets = ticketService.getAllTickets();
        for (Ticket ticket : tickets) {
            bucket.add(ticket.getId());
        }

        List<Airplane> airplanes = airplaneService.getAllAirplanes();
        for (Airplane airplane : airplanes) {
            bucket.add(airplane.getId());
        }

        List<Customer> customers = customerService.getAllCustomers();
        for (Customer customer : customers) {
            bucket.add(customer.getId());
        }
    }

    /**
     * non-argument constructor
     */
    public IdGenerator() {
        super();
    }

    /**
     * Get next ID
     */
    public static synchronized long increment() {

        boolean notDuplicated;
        long randValue;

        do {
            randValue = MIN + (long) (Math.random() * (MAX - MIN));
            notDuplicated = bucket.add(randValue);
        } while (notDuplicated != true);

        return randValue;
    }
}