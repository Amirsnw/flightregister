/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package service.impl;

import exception.CustomerNotFoundException;
import model.user.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import persistence.impl.CustomerFilePersistence;
import service.IdGenerator;
import service.api.CustomerService;

import java.io.IOException;
import java.util.List;

/**
 * This service-impl class for ticket registration
 *
 * @author Amir Khalighi
 */
@Component("customerService")
public class CustomerServiceImpl implements CustomerService {

    CustomerFilePersistence customerPersistence;

    /**
     * Persistence Constructor injection
     */
    @Autowired
    public CustomerServiceImpl(CustomerFilePersistence customerPersistence) {
        this.customerPersistence = customerPersistence;
    }

    /**
     * Print all customer data
     */
    @Override
    public String stringifyCustomersInfo(List<Customer> customers) {

        StringBuilder contain = new StringBuilder();
        contain.append("{");
        if (customers == null || customers.size() == 0) {
            contain.append(" }");
            return contain.toString();
        }

        for (Customer customer : customers) {
            contain.append("\n\t\"" + customer.getId() + "\": {\n");
            contain.append("\t\t" + customer.toString() + " }");
        }
        contain.append("\n }");

        return contain.toString();
    }

    /**
     * Register customer by detailed parameters
     */
    @Override
    public synchronized Customer registerCustomer(String name, String family, long nationalCode,
                                                  String username) throws IOException {

        Customer customer = new Customer(name, family, nationalCode, username);
        try {
            customer = getCustomerByName(name);
        } catch (CustomerNotFoundException e) {
            /* Creating customer */
            customer.setId(IdGenerator.increment());
            customerPersistence.create(customer);
            System.out.println("## Customer was succesfully written ###");
        }

        return customer;
    }

    /**
     * Register customer by single entity
     */
    @Override
    public synchronized Customer registerCustomer(Customer customer) throws IOException {

        try {
            customer = getCustomerByName(customer.getName());
        } catch (CustomerNotFoundException e) {
            customer.setId(IdGenerator.increment());
            customerPersistence.create(customer);
            System.out.println("## Customer was succesfully written ###");
        }

        return customer;
    }

    /**
     * Get all customers
     */
    @Override
    public List<Customer> getAllCustomers() {

        return customerPersistence.fetchAll();
    }

    /**
     * Get customer by Id
     */
    @Override
    public Customer getCustomerById(long classPk) throws CustomerNotFoundException {

        return customerPersistence.findById(classPk);
    }

    /**
     * Get customer by name
     */
    @Override
    public Customer getCustomerByName(String customerName) throws CustomerNotFoundException {

        return customerPersistence.findByName(customerName);
    }

    /**
     * Get customer by name
     */
    @Override
    public List<Customer> getCustomerByUsername(String username) {

        return customerPersistence.findByUsername(username);
    }
}
