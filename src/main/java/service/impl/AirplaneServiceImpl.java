/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package service.impl;

import baseData.AirplaneType;
import exception.AirplaneIsFullException;
import exception.AirplaneNotFoundException;
import model.flight.Airplane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import persistence.impl.AirplaneFilePersistence;
import service.IdGenerator;
import service.api.AirplaneService;

import java.io.IOException;
import java.util.List;

/**
 * This service-impl class for airplane registration
 *
 * @author Amir Khalighi
 */
@Component("airplaneService")
public class AirplaneServiceImpl implements AirplaneService {

    AirplaneFilePersistence airplanePersistence;

    /**
     * Persistence Constructor injection
     */
    @Autowired
    public AirplaneServiceImpl(AirplaneFilePersistence airplanePersistence) {
        this.airplanePersistence = airplanePersistence;
    }

    /**
     * Print all meta data of airplanes
     */
    @Override
    public String stringifyAirplanesInfo(List<Airplane> airplanes) {

        StringBuilder contain = new StringBuilder();
        contain.append("{");
        if (airplanes == null || airplanes.size() == 0) {
            contain.append(" }");
            return contain.toString();
        }

        for (Airplane airplane : airplanes) {
            contain.append("\n\t\"" + airplane.getId() + "\": {\n");
            contain.append("\t\t" + airplane.toString() + " }");
        }
        contain.append("\n }");

        return contain.toString();
    }

    /**
     * register and persist airplane by all parameters
     */
    @Override
    public synchronized Airplane registerAirplane(AirplaneType airplaneType,
                                                  int capacity, long price, int terminalNumber) throws IOException {

        /* Creating Airplane */
        Airplane airplane = new Airplane(airplaneType, capacity, price, terminalNumber);
        airplane.setId(IdGenerator.increment());
        airplanePersistence.create(airplane);
        System.out.println("## Airplane was succesfully written ###");

        return airplane;
    }

    /**
     * register and persist airplane by single entity
     */
    @Override
    public synchronized Airplane registerAirplane(Airplane airplane) throws IOException {

        /* Registering airplane */
        airplane.setId(IdGenerator.increment());
        airplanePersistence.create(airplane);
        System.out.println("## Airplane was succesfully written ###");

        return airplane;
    }

    /**
     * change the capacity
     */
    @Override
    public synchronized Airplane updateCapacity(long airplaneId, int seat) throws IOException,
            AirplaneNotFoundException, IOException, AirplaneIsFullException {

        /* Finding Airplane */
        Airplane airplane = getAirplaneById(airplaneId);

        int result = airplane.getCapacity() - seat;
        if (result < 0) {
            throw new AirplaneIsFullException();
        }
        airplane.setCapacity(result);

        airplanePersistence.update(airplane);
        System.out.println("## Airplane was succesfully updated ###");

        return airplane;
    }

    /**
     * Get all airplanes
     */
    @Override
    public List<Airplane> getAllAirplanes() {

        return airplanePersistence.fetchAll();
    }

    /**
     * Find airplane by id
     */
    @Override
    public Airplane getAirplaneById(long classPk) throws AirplaneNotFoundException {

        return airplanePersistence.findById(classPk);
    }
}