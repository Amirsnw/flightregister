/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package service.impl;

import baseData.City;
import exception.*;
import model.flight.AirLine;
import model.flight.Airplane;
import model.flight.FlightClass;
import model.ticket.Copon;
import model.ticket.Ticket;
import model.user.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import persistence.impl.TicketFilePersistence;
import service.IdGenerator;
import service.api.AirplaneService;
import service.api.CustomerService;
import service.api.TicketService;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * This service-impl class for ticket registration
 *
 * @author Amir Khalighi
 */
@Service("ticketService")
public class TicketServiceImpl implements TicketService {

    TicketFilePersistence ticketPersistence;
    CustomerService customerService;
    AirplaneService airplaneService;

    /**
     * Persistence Constructor injection
     */
    @Autowired
    public TicketServiceImpl(TicketFilePersistence ticketPersistence) {
        this.ticketPersistence = ticketPersistence;
    }

    /**
     * CustomerService setter injection
     */
    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    /**
     * AirplaneService setter injection
     */
    @Autowired
    public void setAirplaneService(AirplaneService airplaneService) {
        this.airplaneService = airplaneService;
    }

    /**
     * Calculate the off price
     * Will be used by Util
     */
    @Override
    public long calculateOffPrice(Copon copon, long price) {
        return ((100 - copon.getOffPercentage()) / 100) * price;
    }

    /**
     * Print all meta data of a customer's ticket
     * Will be used by Util
     */
    @Override
    public String stringifyTicketsInfo(List<Ticket> tickets) throws InvalidFieldException {

        StringBuilder contain = new StringBuilder();
        contain.append("{");
        if (tickets == null || tickets.size() == 0) {
            contain.append(" }");
            return contain.toString();
        }

        Customer customer = null;
        Airplane airplane = null;
        for (Ticket ticket : tickets) {
            try {
                customer = customerService.getCustomerById(ticket.getCustomerId());
            } catch (CustomerNotFoundException e) {
                System.out.println("Invalid Customer ID");
                throw new InvalidFieldException();
            }
            try {
                airplane = airplaneService.getAirplaneById(ticket.getAirplaneId());
            } catch (AirplaneNotFoundException e) {
                System.out.println("Invalid Airplane ID");
                throw new InvalidFieldException();
            }
            contain.append("\n\t\"" + ticket.getId() + "\": {\n");
            contain.append("\t\t\"Ticket\": {" + ticket.toString() + "},\n");
            contain.append("\t\t\"Customer\": {" + customer.toString() + "},\n");
            contain.append("\t\t\"Airplane\": {" + airplane.toString() + "}\n");
            contain.append("\t}");
        }
        contain.append("\n }");

        return contain.toString();
    }

    /**
     * register and persist ticket by all parameters
     */
    @Override
    public Ticket registerTicket(Date date, Customer customer, City from, City to,
                                 AirLine airLine, long airplaneId, FlightClass flightClass, Copon copon, int seat) throws IOException, AirplaneIsFullException,
            InvalidFieldException {

        Ticket ticket = null;

        try {
            airplaneService.updateCapacity(airplaneId, seat);
        } catch (AirplaneNotFoundException e) {
            throw new InvalidFieldException();
        }

        synchronized (TicketServiceImpl.class) {
            customer = customerService.registerCustomer(customer);

            /* Creating Ticket */
            ticket = new Ticket(date, customer.getId(), from, to,
                    copon, airLine, airplaneId, flightClass);
            ticket.setId(IdGenerator.increment());
            ticketPersistence.create(ticket);
        }
        System.out.println("##### Ticket was succesfully written #####");

        return ticket;
    }

    /**
     * register and persist ticket by single entity
     */
    @Override
    public Ticket registerTicket(Ticket ticket, int seat) throws IOException, AirplaneIsFullException,
            InvalidFieldException {

        try {
            airplaneService.updateCapacity(ticket.getAirplaneId(), seat);
        } catch (AirplaneNotFoundException e) {
            throw new InvalidFieldException();
        }

        synchronized (TicketServiceImpl.class) {
            try {
                Customer customer = customerService.getCustomerById(ticket.getCustomerId());
            } catch (CustomerNotFoundException e) {
                throw new InvalidFieldException();
            }

            /* Registering Ticket */
            ticket.setId(IdGenerator.increment());
            ticketPersistence.create(ticket);
        }
        System.out.println("##### Ticket was succesfully written #####");

        return ticket;
    }

    /**
     * Get all tickets
     */
    @Override
    public List<Ticket> getAllTickets() {

        List<Ticket> tickets = ticketPersistence.fetchAll();

        return tickets;
    }

    /**
     * Find ticket by id
     */
    @Override
    public Ticket getTicketById(long classPk) throws TicketNotFoundException {

        Ticket ticket = ticketPersistence.findById(classPk);

        return ticket;
    }

    //**************************

    /**
     * Get tickets by customer name
     */
    @Override
    public List<Ticket> getTicketsByCustomerName(String customerName) throws CustomerNotFoundException {

        Customer customer = customerService.getCustomerByName(customerName);
        List<Ticket> tickets = ticketPersistence.findByCustomerId(customer.getId());

        return tickets;
    }

    /**
     * Get tickets by customer name
     */
    @Override
    public List<Ticket> getTicketsByUsername(String username) {

        List<Ticket> dataResult = new LinkedList<>();

        List<Customer> customers = customerService.getCustomerByUsername(username);
        for (Customer customer : customers) {
            List<Ticket> tickets = ticketPersistence.findByCustomerId(customer.getId());
            for (Ticket ticket : tickets) {
                dataResult.add(ticket);
            }
        }

        return dataResult;
    }

    /**
     * sort All by customer name
     */
    @Override
    public List<Ticket> sortByName() {

        return ticketPersistence.sortByCustomerName(null);
    }

    /**
     * sort by customer name
     */
    @Override
    public List<Ticket> sortByName(List<Ticket> tickets) {

        return ticketPersistence.sortByCustomerName(tickets);
    }

    /**
     * sorts All by id
     */
    @Override
    public List<Ticket> sortByBookingDate() {

        return ticketPersistence.sortByCreationDate(null);
    }

    /**
     * sorts All by id
     */
    @Override
    public List<Ticket> sortByBookingDate(List<Ticket> tickets) {

        return ticketPersistence.sortByCreationDate(tickets);
    }
}
