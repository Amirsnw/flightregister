package service.impl;

import exception.CustomerNotFoundException;
import exception.UserNotFoundException;
import model.front.security.User;
import model.user.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import persistence.impl.UserFilePersistence;
import service.IdGenerator;
import service.api.UserService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService, UserDetailsService {

    UserFilePersistence userPersistence;

    /**
     * Persistence Constructor injection
     */
    @Autowired
    public UserServiceImpl(UserFilePersistence userPersistence) {
        this.userPersistence = userPersistence;
    }

    @Override
    public User registerCustomer(String firstName, String lastName, String username, String password,
                                     String email, String role) throws IOException {

        User user = new User(firstName, lastName, username, password, email, role);
        try {
            getByUsernameOrEmail(username);
        } catch (UserNotFoundException e) {
            /* Creating customer */
            user.setId(IdGenerator.increment());
            userPersistence.create(user);
            System.out.println("## User was succesfully Added ###");
        }

        return user;
    }

    @Override
    public User registerCustomer(User user) throws IOException {
        try {
            getByUsernameOrEmail(user.getUsername());
        } catch (UserNotFoundException e) {
            user.setId(IdGenerator.increment());
            userPersistence.create(user);
            System.out.println("## User was succesfully Added ###");
        }

        return user;
    }

    @Override
    public User getByUsernameOrEmail(String usernameOrEmail) throws UserNotFoundException {

            User user = userPersistence.findByUsernameOrEmail(usernameOrEmail);

            return user;
    }

    @Override
    public User getById(Long id) throws UserNotFoundException {

        User user = userPersistence.findById(id);

        return user;
    }


    @Override
    public UserDetails loadUserByUsername(String usernameOrEmail)
            throws UsernameNotFoundException {

        if (usernameOrEmail.trim().isEmpty()) {
            throw new UsernameNotFoundException("Username Is Empty!");
        }

        User user = null;
        try {
            user = getByUsernameOrEmail(usernameOrEmail);
        } catch (UserNotFoundException e) {
            throw new UsernameNotFoundException("AuthenticationRequest " + usernameOrEmail + "Not Found");
        }

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                getGrantedAuthorities(user));
    }

    private List<GrantedAuthority> getGrantedAuthorities(User user) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        String role = user.getRole();
        authorities.add(new SimpleGrantedAuthority(role));
        return authorities;
    }
}
