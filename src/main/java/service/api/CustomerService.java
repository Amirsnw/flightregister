/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package service.api;

import exception.CustomerNotFoundException;
import model.user.Customer;

import java.io.IOException;
import java.util.List;

/**
 * This service-api interface for customer
 *
 * @author Amir Khalighi
 */
public interface CustomerService {
    public String stringifyCustomersInfo(List<Customer> customers);

    public Customer registerCustomer(String name, String family, long nationalCode,
                                     String username) throws IOException;

    public Customer registerCustomer(Customer customer) throws IOException;

    public List<Customer> getAllCustomers();

    public Customer getCustomerById(long classPk) throws CustomerNotFoundException;

    public Customer getCustomerByName(String customerName) throws CustomerNotFoundException;

    public List<Customer> getCustomerByUsername(String username);
}