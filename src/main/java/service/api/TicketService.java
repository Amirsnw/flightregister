/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package service.api;

import baseData.City;
import exception.AirplaneIsFullException;
import exception.CustomerNotFoundException;
import exception.InvalidFieldException;
import exception.TicketNotFoundException;
import model.flight.AirLine;
import model.flight.FlightClass;
import model.ticket.Copon;
import model.ticket.Ticket;
import model.user.Customer;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * This service-api interface for ticket registration
 *
 * @author Amir Khalighi
 */
public interface TicketService {
    public Ticket registerTicket(Date date, Customer customer, City from, City to,
                                 AirLine airLine, long airplaneId, FlightClass flightClass, Copon copon, int number) throws
            IOException, AirplaneIsFullException, InvalidFieldException;

    public Ticket registerTicket(Ticket ticket, int seat) throws IOException, AirplaneIsFullException,
            InvalidFieldException;

    public String stringifyTicketsInfo(List<Ticket> tickets) throws InvalidFieldException;

    public long calculateOffPrice(Copon copon, long price);

    public List<Ticket> getAllTickets();

    public Ticket getTicketById(long classPk) throws TicketNotFoundException;

    public List<Ticket> getTicketsByCustomerName(String fullName) throws CustomerNotFoundException;

    public List<Ticket> getTicketsByUsername(String username);

    public List<Ticket> sortByName();

    public List<Ticket> sortByName(List<Ticket> tickets);

    public List<Ticket> sortByBookingDate();

    public List<Ticket> sortByBookingDate(List<Ticket> tickets);
}