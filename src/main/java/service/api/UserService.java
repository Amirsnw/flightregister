package service.api;


import exception.UserNotFoundException;
import model.front.security.User;
import model.user.Customer;

import java.io.IOException;

public interface UserService {

    User getByUsernameOrEmail(String usermaneOrEmail) throws UserNotFoundException;

    User getById(Long id) throws UserNotFoundException;

    public User registerCustomer(String firstName, String lastName, String username,
                                     String password, String email, String role) throws IOException;

    public User registerCustomer(User user) throws IOException;
}
