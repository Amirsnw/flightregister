/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package service.api;

import baseData.AirplaneType;
import exception.AirplaneIsFullException;
import exception.AirplaneNotFoundException;
import model.flight.Airplane;

import java.io.IOException;
import java.util.List;

/**
 * This service-api interface for ticket registration
 *
 * @author Amir Khalighi
 */
public interface AirplaneService {
    public String stringifyAirplanesInfo(List<Airplane> airplanes);

    public Airplane registerAirplane(AirplaneType airplaneType, int capacity,
                                     long price, int terminalNumber) throws IOException;

    public Airplane registerAirplane(Airplane airplane) throws IOException;

    public List<Airplane> getAllAirplanes();

    public Airplane getAirplaneById(long classPk) throws AirplaneNotFoundException;

    public Airplane updateCapacity(long airplaneId, int seat)
            throws IOException, AirplaneNotFoundException, AirplaneIsFullException;
}