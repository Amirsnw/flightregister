/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package exception;

/**
 * This Exception will be thrown if the given primary
 * key is duplicated.
 *
 * @author Amir Khalighi
 */
public class InvalidFieldException extends Exception {

    private final String[] fields;

    public InvalidFieldException() {
        super("Filed/s Are Invalid");
        fields = new String[0];
    }

    public InvalidFieldException(String... fields) {
        super("Filed/s Are Invalid ");
        this.fields = fields;
    }

    @Override
    public String toString() {
        StringBuilder content = new StringBuilder();
        content.append("InvalidFieldException [ " + this.getMessage() + " ] {\n");

        for (String field : fields) {
            content.append("		" + field + "\n");
        }

        content.append("	}");

        return content.toString();
    }
}