/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package exception;

/**
 * This Exception will be thrown if the requested file has been modified by third-party.
 *
 * @author Amir Khalighi
 */
public class FileModifiedException extends Exception {

    public FileModifiedException(String fileName) {
        super("The File Has Been Modified : " + fileName);
    }

    @Override
    public String toString() {
        return "FileModifiedException [ " + this.getMessage() + " ]";
    }
}