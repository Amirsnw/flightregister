/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package exception;

/**
 * This Exception will be thrown if the given primary
 * key doesn't match any AuthenticationRequest entity.
 *
 * @author Amir Khalighi
 */
public class UserNotFoundException extends FlightNotFoundException {

    public UserNotFoundException(long primaryKey) {
        super("There is No AuthenticationRequest With The Primary Key = " + primaryKey);
    }

    public UserNotFoundException(String name) {
        super("There is No AuthenticationRequest With = " + name);
    }

    @Override
    public String toString() {
        return "UserNotFoundException [ " + this.getMessage() + " ]";
    }
}
