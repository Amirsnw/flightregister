/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package exception;

/**
 * This Exception will be thrown if the given primary
 * key doesn't match any Customer entity.
 *
 * @author Amir Khalighi
 */
public class CustomerNotFoundException extends FlightNotFoundException {

    public CustomerNotFoundException(long primaryKey) {
        super("There is No Customer With The Primary Key = " + primaryKey);
    }

    public CustomerNotFoundException(String name) {
        super("There is No Customer With The Name/Username = " + name);
    }

    @Override
    public String toString() {
        return "CustomerNotFoundException [ " + this.getMessage() + " ]";
    }
}