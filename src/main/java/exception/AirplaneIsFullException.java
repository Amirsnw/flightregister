/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package exception;

/**
 * This Exception will be thrown if the given primary
 * key is duplicated.
 *
 * @author Amir Khalighi
 */
public class AirplaneIsFullException extends Exception {

    public AirplaneIsFullException() {
        super("The Airplane Is full");
    }

    @Override
    public String toString() {
        return "AirplaneIsFullException [ " + this.getMessage() + " ]";
    }
}