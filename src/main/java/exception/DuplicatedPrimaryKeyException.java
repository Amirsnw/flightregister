/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package exception;

/**
 * This Exception will be thrown if the given primary
 * key is duplicated.
 *
 * @author Amir Khalighi
 */
public class DuplicatedPrimaryKeyException extends Exception {

    public DuplicatedPrimaryKeyException(long primaryKey) {
        super("The Primary Key Has Been Used Before = " + primaryKey);
    }

    @Override
    public String toString() {
        return "DuplicatedPrimaryKeyException [ " + this.getMessage() + " ]";
    }
}