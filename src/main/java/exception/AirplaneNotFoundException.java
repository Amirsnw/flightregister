/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package exception;

/**
 * This Exception will be thrown if the given primary
 * key doesn't match any Airplane entity.
 *
 * @author Amir Khalighi
 */
public class AirplaneNotFoundException extends FlightNotFoundException {

    public AirplaneNotFoundException(long primaryKey) {
        super("There is No Airplane With The Primary Key = " + primaryKey);
    }

    public AirplaneNotFoundException(String name) {
        super("There is No Airplane With The Name = " + name);
    }

    @Override
    public String toString() {
        return "AirplaneNotFoundException [ " + this.getMessage() + " ]";
    }
}