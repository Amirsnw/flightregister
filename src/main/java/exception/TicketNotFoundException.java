/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package exception;

/**
 * This Exception will be thrown if the given primary
 * key doesn't match any Ticket entity.
 *
 * @author Amir Khalighi
 */
public class TicketNotFoundException extends FlightNotFoundException {

    public TicketNotFoundException(long primaryKey) {
        super("There is No Ticket With The Primary Key = " + primaryKey);
    }

    public TicketNotFoundException(String name) {
        super("There is No Ticket With Customer Name = " + name);
    }

    @Override
    public String toString() {
        return "TicketNotFoundException [ " + this.getMessage() + " ]";
    }
}