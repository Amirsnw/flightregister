package config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/* This class is like dispatcher-servlet.xml */
@Configuration
@EnableWebMvc //<mvc:annotation-driven/>
@Import({ BackendConfig.class, SecurityConfig.class })
@ComponentScan(basePackages = { "controller"}) //<context:component-scan base-package="controller"/>
public class WebConfig implements WebMvcConfigurer {

    // define a bean for viewResolver

    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();

        viewResolver.setPrefix("/front/");
        viewResolver.setSuffix(".html");

        return viewResolver;
    }

    @Override
    public void addResourceHandlers( ResourceHandlerRegistry registry )
    {
        registry
                .addResourceHandler( "/resources/**" )
                .addResourceLocations( "/resources/" );
        registry
                .addResourceHandler( "/front/**" )
                .addResourceLocations( "/front/" );
    }
}
