package config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * This is the Config class for all base beans.
 *
 * @author Amir Khalighi
 */

@ComponentScan(basePackages = {"service", "persistence"})
@Configuration
public class BackendConfig {
}
