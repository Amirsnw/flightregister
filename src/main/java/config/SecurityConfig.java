package config;

import controller.filter.AuthTokenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import service.api.UserService;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = {"service", "persistence"})
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@Import({ AuthTokenConfig.class })
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    UserService userService;

    @Autowired
    private AuthTokenConfig authTokenConfig;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        User.UserBuilder users = User.withDefaultPasswordEncoder();
        auth.inMemoryAuthentication()
                .withUser(users.username("amir").password("123456").roles("USER"))
                .withUser(users.username("nima").password("123456").roles("ADMIN"));

        userService.registerCustomer("Amir", "Khalighi", "amir", "123456",
                "amirsnw@gmail.com", "USER");
        userService.registerCustomer("Nima", "Ghazaii", "nima", "123456",
                "nimai@gmail.com", "ADMIN");


        /*String password = bCryptPasswordEncoder().encode("123456");

        auth.inMemoryAuthentication().withUser("amir").password(password).roles("user");
        auth.inMemoryAuthentication().withUser("nima").password(password).roles("admin");*/

        /*AuthenticationRequest.UserBuilder users = AuthenticationRequest.withDefaultPasswordEncoder();
        auth.inMemoryAuthentication().passwordEncoder(bCryptPasswordEncoder())
                .withUser("amir").password("123456").roles("user");*/

        /*BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String password = "123456";
        String encodedPassword = passwordEncoder.encode(password);*/

        /*auth.userDetailsService(userService).passwordEncoder(bCryptPasswordEncoder());
        UserDetails user = auth.getDefaultUserDetailsService().loadUserByUsername("amir");
        System.out.println(user.getPassword());*/
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        {
            /* ***************************** */
            /* PreAuthTokenHeaderFilter filter = new PreAuthTokenHeaderFilter(authHeaderName);
            filter.setAuthenticationManager(new AuthenticationManager()
            {
                @Override
                public Authentication authenticate(Authentication authentication)
                        throws AuthenticationException
                {
                    String principal = (String) authentication.getPrincipal();

                    if (!authHeaderValue.equals(principal))
                    {
                        throw new BadCredentialsException("The API key was not found "
                                + "or not the expected value.");
                    }
                    authentication.setAuthenticated(true);
                    return authentication;
                }
            });*/

            /* ***************************** */
            /* httpSecurity.authorizeRequests()
                    .anyRequest().authenticated() //user must be loged in for any request
                .and().formLogin().loginPage("/login").loginProcessingUrl("/authenticateTheUser")
                    .defaultSuccessUrl("/index.jsp", true)
                .permitAll();*/

            httpSecurity.csrf().disable().authorizeRequests()
                    .antMatchers("/","/api/authenticate").permitAll()
                    .antMatchers(("/user/**")).hasRole("USER")
                    .antMatchers(("/admin/**")).hasRole("ADMIN")
                    .and().formLogin().loginPage("/login")//.loginProcessingUrl("/authenticateit")
                    .defaultSuccessUrl("/index.jsp", true)
                    .permitAll().and().logout().deleteCookies("JSESSIONID").logoutUrl("/api/logout");

            /* ***************************** */
            /*httpSecurity.
                    antMatcher("/api/**")
                    .csrf()
                    .disable()
                    .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                    .addFilter(filter)
                    .addFilterBefore(new ExceptionTranslationFilter(
                                    new Http403ForbiddenEntryPoint()),
                            filter.getClass()
                    )
                    .authorizeRequests()
                    .anyRequest()
                    .authenticated();*/
            httpSecurity.apply(authTokenConfig);
        }
    }

    /*@Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
    */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
