/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package baseData;

/**
 * Created this entity as it is frequent to be used by
 * other program classes.
 *
 * @author Amir Khalighi
 */
public enum City {
    TEHRAN, CHABAHAR
}
