/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package persistence.impl;

import exception.CustomerNotFoundException;
import exception.FileModifiedException;
import model.user.Customer;
import org.springframework.stereotype.Repository;
import persistence.api.Persistence;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * File persistence implementation for customer model
 *
 * @author Amir Khalighi
 */
@Repository
public class CustomerFilePersistence implements Persistence<Customer> {

    private final String filepath = "..\\webapps\\flight-system\\customer.txt";
    private File file = new File(filepath);
    private Set<Customer> bucket = ConcurrentHashMap.newKeySet();
    private final int FIELDS_NUMBER = 5;

    {
        // Caching file from file system on the first run.
        try (Scanner scanner = new Scanner(file)) {
            Customer customer = null;
            Map<String, String> customerStruct = new HashMap<>();

            while (scanner.hasNextLine()) {

                String line = scanner.nextLine();

                try {
                    customerStruct = Arrays.stream(line.split(", ")).map(entry -> entry.split("=")).
                            collect(Collectors.toMap(entry -> entry[0], entry -> entry[1]));
                } catch (ArrayIndexOutOfBoundsException e) {
                    continue;
                }
                customer = parseCustomer(customerStruct);

                bucket.add(customer);
            }
        } catch (FileNotFoundException | FileModifiedException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Non-Parameter Constructor
     */
    public CustomerFilePersistence() {
        super();
    }

    /**
     * Compare-types container
     */
    static class CompareBy {
        private Comparator<Customer> id() {
            return (t1, t2) -> (int) (t1.getId() - t2.getId());
        }

        private Comparator<Customer> customerName() {
            return (t1, t2) -> t1.getName().toLowerCase().
                    compareTo(t2.getName().toLowerCase());
        }
    }

    /**
     * Save cached data
     */
    private void save() throws IOException {

        StringBuilder content = new StringBuilder();

        for (Customer customer : bucket) {
            content.append(customer.toString());
            content.append(System.lineSeparator());
        }

        try (FileWriter fileWriter = new FileWriter(file)) { // appends file
            fileWriter.write(content.toString());
            fileWriter.flush();
        }
    }

    /**
     * This method cache and save customer
     */
    @Override
    public Customer create(Customer customer) throws IOException {

        if (customer == null) {
            throw new NullPointerException();
        }

        bucket.add(customer);
        save();

        return customer;
    }

    /**
     * Update customer
     */
    @Override
    public Customer update(Customer customer) throws IOException, CustomerNotFoundException {
        if (customer == null) {
            throw new NullPointerException();
        }
        return null;
    }

    /**
     * Get all customers
     */
    @Override
    public List<Customer> fetchAll() {

        List<Customer> dataStruct = new LinkedList<>(bucket);

        CompareBy compareBy = new CompareBy();
        Collections.sort(dataStruct, compareBy.id());

        return dataStruct;
    }

    /**
     * find customer by Id
     */
    @Override
    public Customer findById(long classPk) throws CustomerNotFoundException {

        for (Customer customer : bucket) {
            if (customer.getId() == classPk) {
                return customer;
            }
        }

        throw new CustomerNotFoundException(classPk);
    }


    /**
     * Find customer by name
     */
    public Customer findByName(String customerName) throws CustomerNotFoundException {

        for (Customer customer : bucket) {
            if (customer.getName().equalsIgnoreCase(customerName)) {
                return customer;
            }
        }

        throw new CustomerNotFoundException(customerName);
    }

    public List<Customer> findByUsername(String username) {

        List<Customer> dataResult = new LinkedList<>();

        for (Customer customer : bucket) {
            if (customer.getUsername().equalsIgnoreCase(username)) {
                dataResult.add(customer);
            }
        }

        return dataResult;
    }

    public List<Customer> sortByName(List<Customer> customers) {

        if (customers == null || customers.size() == 0) {
            customers = new LinkedList<>(bucket);
        }

        CompareBy compareBy = new CompareBy();
        Collections.sort(customers, compareBy.customerName());

        return customers;
    }

    private Customer parseCustomer(Map<String, String> customerStruct) throws FileModifiedException {

        if (customerStruct.size() != FIELDS_NUMBER) {
            throw new FileModifiedException(file.getName());
        }

        Customer customer = new Customer();
        try {
            customer.setId(Long.parseLong(customerStruct.get("customerId")));
            customer.setName(customerStruct.get("customerName"));
            customer.setFamily(customerStruct.get("customerFamily"));
            customer.setNationalCode(Long.parseLong(customerStruct.get("customerNationalCode")));
            customer.setUsername(customerStruct.get("username"));
        } catch (NumberFormatException e) {
            throw new FileModifiedException(file.getName());
        }

        return customer;
    }
}