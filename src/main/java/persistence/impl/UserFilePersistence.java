/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package persistence.impl;

import exception.FileModifiedException;
import exception.UserNotFoundException;
import model.front.security.User;
import org.springframework.stereotype.Repository;
import persistence.api.Persistence;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * File persistence implementation for Ticket model
 *
 * @author Amir Khalighi
 */
@Repository
public class UserFilePersistence implements Persistence<User> {

    private final String filepath = "..\\webapps\\flight-system\\user.txt";
    private final int FIELDS_NUMBER = 7;
    private File file = new File(filepath);
    private Set<User> bucket = ConcurrentHashMap.newKeySet();

    {
        // Caching file from file system on the first run.
        try (Scanner scanner = new Scanner(file)) {
            User user = null;
            Map<String, String> userStruct = new HashMap<>();

            while (scanner.hasNextLine()) {

                String line = scanner.nextLine();

                try {
                    userStruct = Arrays.stream(line.split(", ")).map(entry -> entry.split("=")).
                            collect(Collectors.toMap(entry -> entry[0], entry -> entry[1]));
                } catch (ArrayIndexOutOfBoundsException e) {
                    continue;
                }

                user = parseUser(userStruct);

                bucket.add(user);
            }
        } catch (FileNotFoundException | FileModifiedException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Non-Parameter Constructor
     */
    public UserFilePersistence() {
        super();
    }

    /**
     * Save cached data
     */
    private void save() throws IOException {

        StringBuilder content = new StringBuilder();

        for (User user : bucket) {
            content.append(user.toString());
            content.append(System.lineSeparator());
        }

        try (FileWriter fileWriter = new FileWriter(file)) { // appends file
            fileWriter.write(content.toString());
            fileWriter.flush();
        }
    }

    /**
     * This method cache and save user
     */
    @Override
    public User create(User user) throws IOException {

        if (user == null) {
            throw new NullPointerException();
        }

        bucket.add(user);
        save();

        return user;
    }

    /**
     * Update user
     */
    @Override
    public User update(User user) throws IOException, UserNotFoundException {
        return null;
    }

    /**
     * Find user by customer name
     */
    @Override
    public List<User> fetchAll() {
        return null;
    }

    /**
     * find Ticket by Id
     */
    @Override
    public User findById(long classPk) throws UserNotFoundException {

        for (User user : bucket) {
            if (user.getId() == classPk) {
                return user;
            }
        }

        throw new UserNotFoundException(classPk);
    }

    public User findByUsernameOrEmail(String UsernameOrEmail) throws UserNotFoundException {

        for (User user : bucket) {
            if (user.getUsername().equalsIgnoreCase(UsernameOrEmail)) {
                return user;
            }
            if (user.getEmail().equalsIgnoreCase(UsernameOrEmail)) {
                return user;
            }
        }

        throw new UserNotFoundException(UsernameOrEmail);
    }

    private User parseUser(Map<String, String> userStruct) throws FileModifiedException {

        if (userStruct.size() != FIELDS_NUMBER) {
            throw new FileModifiedException(file.getName());
        }

        User user = new User();
        try {
            user.setId(Long.parseLong(userStruct.get("id")));
        } catch (NumberFormatException e) {
            throw new FileModifiedException(file.getName());
        }
            user.setFirstName(userStruct.get("firstName"));
            user.setLastName(userStruct.get("lastName"));
            user.setUsername(userStruct.get("username"));
            user.setPassword(userStruct.get("password"));
            user.setEmail(userStruct.get("email"));
            user.setRole(userStruct.get("role"));

        return user;
    }
}
