/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package persistence.impl;

import baseData.AirplaneType;
import exception.AirplaneNotFoundException;
import exception.FileModifiedException;
import model.flight.Airplane;
import org.springframework.stereotype.Repository;
import persistence.api.Persistence;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * File persistence implementation for Airplane model
 *
 * @author Amir Khalighi
 */
@Repository
public class AirplaneFilePersistence implements Persistence<Airplane> {

    private final String filepath = "..\\webapps\\flight-system\\airplane.txt";
    private File file = new File(filepath);
    private Set<Airplane> bucket = ConcurrentHashMap.newKeySet();
    private final int FIELDS_NUMBER = 5;

    {

        // Caching file from file system on the first run.
        try (Scanner scanner = new Scanner(file)) {
            Airplane airplane = null;
            Map<String, String> airplaneStruct = new HashMap<>();

            while (scanner.hasNextLine()) {

                String line = scanner.nextLine();

                try {
                    airplaneStruct = Arrays.stream(line.split(", ")).map(entry -> entry.split("=")).
                            collect(Collectors.toMap(entry -> entry[0], entry -> entry[1]));
                } catch (ArrayIndexOutOfBoundsException e) {
                    continue;
                }
                airplane = parseAirplane(airplaneStruct);

                bucket.add(airplane);
            }
        } catch (FileNotFoundException | FileModifiedException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Non-Parameter Constructor
     */
    public AirplaneFilePersistence() {
        super();
    }

    /**
     * Save cached data
     */
    private void save() throws IOException {

        StringBuilder content = new StringBuilder();

        for (Airplane airplane : bucket) {
            content.append(airplane.toString());
            content.append(System.lineSeparator());
        }

        try (FileWriter fileWriter = new FileWriter(file)) { // appends file
            fileWriter.write(content.toString());
            fileWriter.flush();
        }
    }

    /**
     * This method cache and save airplane
     */
    @Override
    public Airplane create(Airplane airplane) throws IOException {

        if (airplane == null) {
            throw new NullPointerException();
        }

        bucket.add(airplane);
        save();

        return airplane;
    }

    /**
     * Update airplane
     */
    @Override
    public Airplane update(Airplane airplane) throws IOException, AirplaneNotFoundException {

        boolean found = false;
        for (Airplane ap : bucket) {
            if (ap.getId() == airplane.getId()) {
                found = true;
                bucket.remove(airplane);
                bucket.add(airplane);
            }
        }

        if (found == false) {
            throw new AirplaneNotFoundException(airplane.getId());
        }

        save();
        return airplane;
    }

    /**
     * Get all airplanes
     */
    @Override
    public List<Airplane> fetchAll() {

        List<Airplane> dataStruct = new LinkedList<>(bucket);

        return dataStruct;
    }

    /**
     * find airplane by Id
     */
    @Override
    public Airplane findById(long classPk) throws AirplaneNotFoundException {

        for (Airplane airplane : bucket) {
            if (airplane.getId() == classPk) {
                return airplane;
            }
        }

        throw new AirplaneNotFoundException(classPk);
    }

    private Airplane parseAirplane(Map<String, String> airplaneStruct) throws FileModifiedException {

        if (airplaneStruct.size() != FIELDS_NUMBER) {
            throw new FileModifiedException(file.getName());
        }

        Airplane airplane = new Airplane();
        try {
            airplane.setId(Long.parseLong(airplaneStruct.get("id")));
            airplane.setAirplaneType(AirplaneType.valueOf(airplaneStruct.get("airplaneType")));
            airplane.setCapacity(Integer.parseInt(airplaneStruct.get("capacity")));
            airplane.setTerminalNumber(Integer.parseInt(airplaneStruct.get("terminalNumber")));
            airplane.setPrice(Long.parseLong(airplaneStruct.get("price")));
        } catch (NumberFormatException e) {
            throw new FileModifiedException(file.getName());
        }

        return airplane;
    }
}