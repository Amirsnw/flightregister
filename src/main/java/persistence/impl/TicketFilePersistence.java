/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package persistence.impl;

import baseData.City;
import exception.FileModifiedException;
import exception.TicketNotFoundException;
import model.flight.AirLine;
import model.flight.FlightClass;
import model.ticket.Copon;
import model.ticket.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import persistence.TicketCompareUtil;
import persistence.api.Persistence;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * File persistence implementation for Ticket model
 *
 * @author Amir Khalighi
 */
@Repository
public class TicketFilePersistence implements Persistence<Ticket> {

    private final String filepath = "..\\webapps\\flight-system\\ticket.txt";
    private File file = new File(filepath);
    private Set<Ticket> bucket = ConcurrentHashMap.newKeySet();
    private final int FIELDS_NUMBER = 11;
    TicketCompareUtil compareBy;


    {
        // Caching file from file system on the first run.
        try (Scanner scanner = new Scanner(file)) {
            Ticket ticket = null;
            Map<String, String> ticketStruct = new HashMap<>();

            while (scanner.hasNextLine()) {

                String line = scanner.nextLine();

                try {
                    ticketStruct = Arrays.stream(line.split(", ")).map(entry -> entry.split("=")).
                            collect(Collectors.toMap(entry -> entry[0], entry -> entry[1]));
                } catch (ArrayIndexOutOfBoundsException e) {
                    continue;
                }

                ticket = parseTicket(ticketStruct);

                bucket.add(ticket);
            }
        } catch (FileNotFoundException | FileModifiedException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Non-Parameter Constructor
     */
    public TicketFilePersistence() {
        super();
    }

    /**
     * ticketCompareUtil setter injection
     */
    @Autowired
    public void setCompareBy(TicketCompareUtil compareBy) {
        this.compareBy = compareBy;
    }

    /**
     * Save cached data
     */
    private void save() throws IOException {

        StringBuilder content = new StringBuilder();

        for (Ticket ticket : bucket) {
            content.append(ticket.toString());
            content.append(System.lineSeparator());
        }

        try (FileWriter fileWriter = new FileWriter(file)) { // appends file
            fileWriter.write(content.toString());
            fileWriter.flush();
        }
    }

    /**
     * This method cache and save ticket
     */
    @Override
    public Ticket create(Ticket ticket) throws IOException {

        if (ticket == null) {
            throw new NullPointerException();
        }

        bucket.add(ticket);
        save();

        return ticket;
    }

    /**
     * Update ticket
     */
    @Override
    public Ticket update(Ticket ticket) throws IOException, TicketNotFoundException {
        if (ticket == null) {
            throw new NullPointerException();
        }
        return null;
    }

    /**
     * Find ticket by customer name
     */
    @Override
    public List<Ticket> fetchAll() {

        List<Ticket> dataStruct = new LinkedList<>(bucket);

        Collections.sort(dataStruct, compareBy.id());

        return dataStruct;
    }

    /**
     * find Ticket by Id
     */
    @Override
    public Ticket findById(long classPk) throws TicketNotFoundException {

        for (Ticket ticket : bucket) {
            if (ticket.getId() == classPk) {
                return ticket;
            }
        }

        throw new TicketNotFoundException(classPk);
    }


    /**
     * Find ticket by customer name
     */
    public List<Ticket> findByCustomerId(long customerId) {

        List<Ticket> dataResult = new LinkedList<>();

        for (Ticket ticket : bucket) {
            if (ticket.getCustomerId() == customerId) {
                dataResult.add(ticket);
            }
        }

        Collections.sort(dataResult, compareBy.id());

        return dataResult;
    }

    public List<Ticket> sortByCustomerName(List<Ticket> tickets) {

        if (tickets == null || tickets.size() == 0) {
            tickets = new LinkedList<>(bucket);
        }

        Collections.sort(tickets, compareBy.customerName());

        return tickets;
    }

    public List<Ticket> sortByCreationDate(List<Ticket> tickets) {

        if (tickets == null || tickets.size() == 0) {
            tickets = new LinkedList<>(bucket);
        }

        Collections.sort(tickets, compareBy.createDate());

        return tickets;
    }

    private Ticket parseTicket(Map<String, String> ticketStruct) throws FileModifiedException {

        if (ticketStruct.size() != FIELDS_NUMBER) {
            throw new FileModifiedException(file.getName());
        }

        DateFormat dateParser = new SimpleDateFormat("dd-M-yyyy hh mm ss");

        Ticket ticket = new Ticket();
        try {
            ticket.setId(Long.parseLong(ticketStruct.get("id")));
            ticket.setDate(dateParser.parse(ticketStruct.get("flightDate")));

            ticket.setCustomerId(Long.parseLong(ticketStruct.get("customerId")));
            ticket.setAirLine(new AirLine(ticketStruct.get("airLineName")));
            ticket.setFlightClass(FlightClass.valueOf(ticketStruct.get("flightClass")));
            ticket.setAirplaneId(Long.parseLong(ticketStruct.get("airplaneId")));
            ticket.setFrom(City.valueOf(ticketStruct.get("from")));
            ticket.setTo(City.valueOf(ticketStruct.get("to")));
            ticket.setCopon(new Copon(Integer.parseInt(ticketStruct.get("offPercentage")),
                    ticketStruct.get("coponName")));

            ticket.setCreateDate(dateParser.parse(ticketStruct.get("createDate")));
        } catch (NumberFormatException | ParseException e) {
            throw new FileModifiedException(file.getName());
        }

        return ticket;
    }
}