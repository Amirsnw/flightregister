/*
 * Amir Khalighi
 *
 * Copyright (c) 2020-2030 Snowman
 *
 * This program is witten for Tosan cooperation
 *
 */
package persistence.api;

import exception.FlightNotFoundException;

import java.io.IOException;
import java.util.List;

/**
 * This is a general-purpose persistence api
 *
 * @author Amir Khalighi
 */
public interface Persistence<T> {

    public T create(T entity) throws IOException;

    public T update(T entity) throws IOException, FlightNotFoundException;

    public T findById(long classPk) throws FlightNotFoundException;

    public List<T> fetchAll();
}