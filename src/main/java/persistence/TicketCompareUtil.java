package persistence;

import exception.CustomerNotFoundException;
import model.ticket.Ticket;
import model.user.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import service.api.CustomerService;

import java.util.Comparator;

/**
 * Compare-types container
 */
@Component
public class TicketCompareUtil {

    CustomerService customerService;

    /**
     * CustomerService setter injection
     */
    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    public Comparator<Ticket> id() {
        return (t1, t2) -> (int) (t1.getId() - t2.getId());
    }

    public Comparator<Ticket> customerName() {

        return new Comparator<Ticket>() {
            public int compare(Ticket t1, Ticket t2) {
                Customer customer1 = null;
                Customer customer2 = null;
                try {
                    customer1 = customerService.
                            getCustomerById(t1.getCustomerId());
                    customer2 = customerService.
                            getCustomerById(t2.getCustomerId());
                } catch (CustomerNotFoundException e) {
                    System.err.println("Could not find customer while comparing");
                }
                return customer1.getName().toLowerCase().compareTo(customer2.
                        getName().toLowerCase());
            }
        };
    }

    public Comparator<Ticket> createDate() {
        return (t1, t2) -> t1.getCreateDate().compareTo(t2.getCreateDate());
    }
}
